/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#ifndef KNOB_LOOK_AND_FEEL_H
#define KNOB_LOOK_AND_FEEL_H

#include "../JuceLibraryCode/JuceHeader.h"

class KnobLookAndFeel : public LookAndFeel_V3
{
public:
    KnobLookAndFeel();
    virtual ~KnobLookAndFeel();

    virtual void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle, Slider& slider);

private:
    Image knobStrip;
    int singleImageWidth;
    int singleImageHeight;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(KnobLookAndFeel);
};

#endif // KNOB_LOOK_AND_FEEL_H


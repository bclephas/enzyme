/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "Filter.h"

#include <algorithm>

Filter::Filter()
    : cutoff(0.99)
    , cutoffMod(0.0)
    , resonance(0.0)
    , filterMode(FILTER_MODE_LOWPASS)
    , buf0(0.0)
    , buf1(0.0)
    , buf2(0.0)
    , buf3(0.0)
{
    calculateFeedbackAmount();
}

void Filter::reset()
{
    buf0 = buf1 = buf2 = buf3 = 0.0;
}

// http://www.musicdsp.org/showone.php?id=29
// Two first order low-pass filters in series
double Filter::process(double input)
{
    if (input == 0.0) {
        return input;
    }

    double calculatedCutoff = getCalculatedCutoff();

    // attenuation = -12dB per octave
    buf0 += calculatedCutoff * (input - buf0 + feedbackAmount * (buf0 - buf1));
    buf1 += calculatedCutoff * (buf0 - buf1);

    // attenuation = -24dB per octave
    buf2 += calculatedCutoff * (buf1 - buf2);
    buf3 += calculatedCutoff * (buf2 - buf3);

    double value;
    switch (filterMode) {
        case FILTER_MODE_LOWPASS:
            //value = buf1; // -12dB
            value = buf3; // -24dB
            break;
        case FILTER_MODE_HIGHPASS:
            //value = input - buf0; // -12dB
            value = input - buf3; // -24dB
            break;
        case FILTER_MODE_BANDPASS:
            //value = buf0 - buf1; // -12dB
            value = buf0 - buf3; // -24dB
            break;
        default:
            value = 0.0;
            break;
    }

    return value;
}

void Filter::setCutoff(double cutoff)
{
    this->cutoff = cutoff;
    calculateFeedbackAmount();
}

void Filter::setCutoffMod(double cutoff)
{
    cutoffMod = cutoff;
    calculateFeedbackAmount();
}

void Filter::setResonance(double resonance)
{
    this->resonance = resonance;
    calculateFeedbackAmount();
}

void Filter::setFilterMode(FilterMode mode)
{
    filterMode = mode;
}

void Filter::calculateFeedbackAmount()
{
    feedbackAmount = resonance + (resonance / (1.0 - getCalculatedCutoff()));
}

double Filter::getCalculatedCutoff() const
{
    return std::max(std::min(cutoff + cutoffMod, 0.99), 0.01);
}


/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "Voice.h"

Voice::Voice()
    : noteNumber(-1)
    , velocity(0)
    , filterEnvelopeAmount(0.0)
    , oscillatorMix(0.5)
    , filterLFOAmount(0.0)
    , oscillatorOnePitchAmount(0.0)
    , oscillatorTwoPitchAmount(0.0)
    , lfoValue(0.0)
    , isActive(false)
{
    volumeEnvelope.finishedEnvelopeCycle.Connect(this, &Voice::onFinishedEnvelopeCycle);
}

double noteNumberToFrequency(int noteNumber)
{
    return 440.0 * std::pow(2.0, (noteNumber - 69.0) / 12.0);
}

double Voice::nextSample()
{
    // compute the total output of a voice after volume enveloping and filtering
    if (!isActive) {
        return 0.0;
    }

    double oscillatorOneSample = oscillatorOne.nextSample();
    double oscillatorTwoSample = oscillatorTwo.nextSample();
    double oscillatorSum = ((1.0 - oscillatorMix) * oscillatorOneSample) + (oscillatorMix * oscillatorTwoSample);

    double volumeEnvelopeSample = volumeEnvelope.nextSample();
    double filterEnvelopeSample = filterEnvelope.nextSample();

    filter.setCutoffMod((filterEnvelopeSample * filterEnvelopeAmount) + (lfoValue * filterLFOAmount));

    oscillatorOne.setPitchMod(lfoValue * oscillatorOnePitchAmount);
    oscillatorTwo.setPitchMod(lfoValue * oscillatorTwoPitchAmount);

    double sample = filter.process(oscillatorSum * volumeEnvelopeSample * (velocity / 127.0));

    return sample;
}

void Voice::reset()
{
    noteNumber = -1;
    velocity = 0;
    oscillatorOne.reset();
    oscillatorTwo.reset();
    volumeEnvelope.reset();
    filterEnvelope.reset();
    filter.reset();
}

void Voice::setNoteNumber(int noteNumber)
{
    this->noteNumber = noteNumber;
    double frequency = noteNumberToFrequency(noteNumber);
    oscillatorOne.setFrequency(frequency);
    oscillatorTwo.setFrequency(frequency);
}

void Voice::setFilterEnvelopeAmount(double amount)
{
    filterEnvelopeAmount = amount;
}

void Voice::setFilterLFOAmount(double amount)
{
    filterLFOAmount = amount;
}

void Voice::setOscillatorOnePitchAmount(double amount)
{
    oscillatorOnePitchAmount = amount;
}

void Voice::setOscillatorTwoPitchAmount(double amount)
{
    oscillatorTwoPitchAmount = amount;
}

void Voice::setOscillatorMix(double mix)
{
    oscillatorMix = mix;
}

void Voice::setLFOValue(double value)
{
    lfoValue = value;
}

void Voice::onFinishedEnvelopeCycle()
{
    isActive = false;
}


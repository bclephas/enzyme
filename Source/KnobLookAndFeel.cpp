/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "KnobLookAndFeel.h"
#include "Resources/knob_med_red.h"

KnobLookAndFeel::KnobLookAndFeel()
    : singleImageWidth(1)
    , singleImageHeight(1)
{
    knobStrip = ImageCache::getFromMemory(KNOB_MED_RED_PNG_DATA, KNOB_MED_RED_PNG_SIZE);
}

KnobLookAndFeel::~KnobLookAndFeel()
{
}

void KnobLookAndFeel::drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle, Slider& slider)
{
    // value between 0 and 1 for current amount of rotation
    const double fractionalRotation = (slider.getValue() - slider.getMinimum()) / (slider.getMaximum() - slider.getMinimum());
    const int totalFrames = knobStrip.getHeight()/knobStrip.getWidth();
    const int frameIndex = static_cast<int>(std::ceil(fractionalRotation * static_cast<double>(totalFrames - 1.0)));

    const float radius = jmin(width / 2.0f, height / 2.0f);
    const float centerX = x + width * 0.5f;
    const float centerY = y + height * 0.5f;
    const float rx = centerX - radius;
    const float ry = centerY - radius;

    g.drawImage(knobStrip, rx, ry, 2 * radius, 2 * radius, 0, frameIndex * knobStrip.getWidth(), knobStrip.getWidth(), knobStrip.getWidth());
}


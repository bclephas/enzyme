/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#ifndef VOICE_MANAGER_H
#define VOICE_MANAGER_H

#include "Oscillator.h"
#include "Voice.h"

#include <array>
#include <functional>

class VoiceManager
{
public:
    typedef std::function<void (Voice&)> VoiceParameterChangerFunction;

public:
    VoiceManager();

    double nextSample();
    void setSampleRate(double sampleRate);
    void setLFOMode(Oscillator::OscillatorMode mode);
    void setLFOFrequency(double frequency);

    void updateVoices(VoiceParameterChangerFunction changer);

    void onNoteOn(int noteNumber, int velocity);
    void onNoteOff(int noteNumber, int velocity);

    // voice parameter change functions
    void setVolumeEnvelopeStageValue(Voice& voice, EnvelopeGenerator::EnvelopeStage stage, double value);
    void setFilterEnvelopeStageValue(Voice& voice, EnvelopeGenerator::EnvelopeStage stage, double value);
    void setOscillatorMode(Voice& voice, int oscillatorNumber, Oscillator::OscillatorMode mode);
    void setOscillatorPitchMod(Voice& voice, int oscillatorNumber, double amount);
    void setOscillatorMix(Voice& voice, double mix);
    void setFilterCutoff(Voice& voice, double cutoff);
    void setFilterResonance(Voice& voice, double resonance);
    void setFilterMode(Voice& voice, Filter::FilterMode mode);
    void setFilterEnvelopeAmount(Voice& voice, double amount);
    void setFilterLFOAmount(Voice& voice, double amount);

private:
    Voice* findFirstAvailableVoice();

private:
    static const int VOICES = 64;
    typedef std::array<Voice, VOICES> VoicesArray;
    VoicesArray voices;
    Oscillator lfo;
};

#endif // VOICE_MANAGER_H

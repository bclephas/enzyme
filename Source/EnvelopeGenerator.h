/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#ifndef ENVELOPE_GENERATOR_H
#define ENVELOPE_GENERATOR_H

#include "Signal.h"

#include <cmath>

class EnvelopeGenerator
{
public:
    enum EnvelopeStage {
        ENVELOPE_STAGE_OFF = 0,
        ENVELOPE_STAGE_ATTACK,
        ENVELOPE_STAGE_DECAY,
        ENVELOPE_STAGE_SUSTAIN,
        ENVELOPE_STAGE_RELEASE,
        NumEnvelopeStages
    };

public:
    EnvelopeGenerator();

    void reset();
    void enterStage(EnvelopeStage stage);
    double nextSample();
    void setSampleRate(double sampleRate);
    EnvelopeStage getCurrentStage() const;
    void setEnvelope(EnvelopeStage stage, double value);
    double getEnvelope(EnvelopeStage stage) const;

private:
    void calculateMultiplier(double startLevel, double endLevel, unsigned long long lengthInSamples);

private:
    const double minimumLevel;
    EnvelopeStage currentStage;
    double currentLevel;
    double sampleRate;
    double multiplier;
    double stageValue[NumEnvelopeStages];
    unsigned long long currentSampleIndex;
    unsigned long long nextStageSampleIndex;

public:
    Gallant::Signal0<> startedEnvelopeCycle;
    Gallant::Signal0<> finishedEnvelopeCycle;
};

#endif // ENVELOPE_GENERATOR_H


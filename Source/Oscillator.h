/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#ifndef OSCILLATOR_H
#define OSCILLATOR_H

#include <cmath>

class Oscillator
{
public:
    enum OscillatorMode
    {
        OSCILLATOR_MODE_SINE,
        OSCILLATOR_MODE_SAW,
        OSCILLATOR_MODE_SQUARE,
        OSCILLATOR_MODE_TRIANGLE
    };

public:
    Oscillator();

    void reset();
    void setMode(OscillatorMode mode);
    void setFrequency(double frequency);
    double getFrequency() const;
    void setSampleRate(double sampleRate);
    void setPitchMod(double amount);

    double nextSample();

private:
    void updateIncrement();

private:
    OscillatorMode oscillatorMode;
    double frequency;
    double sampleRate;
    double phase;
    double phaseIncrement;
    double pitchMod;
};

#endif // OSCILLATOR_H


/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#ifndef FILTER_H
#define FILTER_H

class Filter
{
public:
    enum FilterMode {
        FILTER_MODE_LOWPASS = 0,
        FILTER_MODE_HIGHPASS,
        FILTER_MODE_BANDPASS,
        NumFilterModes
    };

public:
    Filter();

    void reset();
    double process(double input);
    void setCutoff(double cutoff);
    void setCutoffMod(double cutoff);
    void setResonance(double resonance);
    void setFilterMode(FilterMode mode);

private:
    void calculateFeedbackAmount();
    double getCalculatedCutoff() const;

private:
    double cutoff;
    double cutoffMod;
    double resonance;
    FilterMode filterMode;
    double feedbackAmount;
    double buf0;
    double buf1;
    double buf2;
    double buf3;
};

#endif // FILTER_H


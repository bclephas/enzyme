/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "PluginProcessor.h"
#include "PluginEditor.h"

#include "Oscillator.h"
#include "EnvelopeGenerator.h"
#include "Filter.h"

const int COMBOBOX_OSCILLATOR_BASE = 0x400;
const int COMBOBOX_FILTER_BASE = 0x500;
const int COMBOBOX_LFO_BASE = 0x500;

//==============================================================================
SynthAudioProcessorEditor::SynthAudioProcessorEditor (SynthAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (500, 200);

    // Main settings
    oscillatorOneModeLabel.setText("Wave", dontSendNotification);
    oscillatorOneModeLabel.setEditable(false);

    oscillatorOneMode.setEditableText(false);
    oscillatorOneMode.addItem("Sine", COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_SINE);
    oscillatorOneMode.addItem("Saw", COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_SAW);
    oscillatorOneMode.addItem("Square", COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_SQUARE);
    oscillatorOneMode.addItem("Triangle", COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_TRIANGLE);
    oscillatorOneMode.setSelectedId(COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_SINE, dontSendNotification);
    oscillatorOneMode.addListener(this);

    oscillatorOnePitchModLabel.setText("Pitch", dontSendNotification);
    oscillatorOnePitchModLabel.setEditable(false);

    oscillatorOnePitchModSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    oscillatorOnePitchModSlider.setRange(0.0, 1.0, 0.01);
    oscillatorOnePitchModSlider.setValue(0.0, dontSendNotification);
    oscillatorOnePitchModSlider.setLookAndFeel(&knobLookAndFeel);
    oscillatorOnePitchModSlider.addListener(this);

    oscillatorMixLabel.setText("Mix", dontSendNotification);
    oscillatorMixLabel.setEditable(false);

    oscillatorMixSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    oscillatorMixSlider.setRange(0.0, 1.0, 0.01);
    oscillatorMixSlider.setValue(0.5, dontSendNotification);
    oscillatorMixSlider.setLookAndFeel(&knobLookAndFeel);
    oscillatorMixSlider.addListener(this);

    oscillatorTwoModeLabel.setText("Wave", dontSendNotification);
    oscillatorTwoModeLabel.setEditable(false);

    oscillatorTwoMode.setEditableText(false);
    oscillatorTwoMode.addItem("Sine", COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_SINE);
    oscillatorTwoMode.addItem("Saw", COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_SAW);
    oscillatorTwoMode.addItem("Square", COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_SQUARE);
    oscillatorTwoMode.addItem("Triangle", COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_TRIANGLE);
    oscillatorTwoMode.setSelectedId(COMBOBOX_OSCILLATOR_BASE + (int)Oscillator::OSCILLATOR_MODE_SINE, dontSendNotification);
    oscillatorTwoMode.addListener(this);

    oscillatorTwoPitchModLabel.setText("Pitch", dontSendNotification);
    oscillatorTwoPitchModLabel.setEditable(false);

    oscillatorTwoPitchModSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    oscillatorTwoPitchModSlider.setRange(0.0, 1.0, 0.01);
    oscillatorTwoPitchModSlider.setValue(0.0, dontSendNotification);
    oscillatorTwoPitchModSlider.setLookAndFeel(&knobLookAndFeel);
    oscillatorTwoPitchModSlider.addListener(this);

    // Volume envelope
    attackLabel.setText("A", dontSendNotification);
    attackLabel.setJustificationType(Justification::horizontallyCentred);
    attackLabel.setEditable(false);

    attackSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    attackSlider.setRange(0.01, 10.0, 0.001);
    attackSlider.setValue(0.01, dontSendNotification);
    attackSlider.setLookAndFeel(&knobLookAndFeel);
    attackSlider.addListener(this);

    decayLabel.setText("D", dontSendNotification);
    decayLabel.setJustificationType(Justification::horizontallyCentred);
    decayLabel.setEditable(false);

    decaySlider.setSliderStyle(Slider::RotaryVerticalDrag);
    decaySlider.setRange(0.01, 15.0, 0.001);
    decaySlider.setValue(0.5, dontSendNotification);
    decaySlider.setLookAndFeel(&knobLookAndFeel);
    decaySlider.addListener(this);

    sustainLabel.setText("S", dontSendNotification);
    sustainLabel.setJustificationType(Justification::horizontallyCentred);
    sustainLabel.setEditable(false);

    sustainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    sustainSlider.setRange(0.001, 1.0, 0.001);
    sustainSlider.setValue(0.1, dontSendNotification);
    sustainSlider.setLookAndFeel(&knobLookAndFeel);
    sustainSlider.addListener(this);

    releaseLabel.setText("R", dontSendNotification);
    releaseLabel.setJustificationType(Justification::horizontallyCentred);
    releaseLabel.setEditable(false);

    releaseSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    releaseSlider.setRange(0.001, 15.0, 0.001);
    releaseSlider.setValue(1.0, dontSendNotification);
    releaseSlider.setLookAndFeel(&knobLookAndFeel);
    releaseSlider.addListener(this);

    cutoffLabel.setText("Cutoff", dontSendNotification);
    cutoffLabel.setEditable(false);

    cutoffSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    cutoffSlider.setRange(0.01, 0.99, 0.001);
    cutoffSlider.setValue(0.8, dontSendNotification);
    cutoffSlider.setLookAndFeel(&knobLookAndFeel);
    cutoffSlider.addListener(this);

    resonanceLabel.setText("Res.", dontSendNotification);
    resonanceLabel.setEditable(false);

    resonanceSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    resonanceSlider.setRange(0.01, 1.0, 0.001);
    resonanceSlider.setValue(0.01, dontSendNotification);
    resonanceSlider.setLookAndFeel(&knobLookAndFeel);
    resonanceSlider.addListener(this);

    // Filter
    filterModeLabel.setText("Mode", dontSendNotification);
    filterModeLabel.setEditable(false);

    filterMode.setEditableText(false);
    filterMode.addItem("Lowpass",  COMBOBOX_FILTER_BASE + (int)Filter::FILTER_MODE_LOWPASS);
    filterMode.addItem("Highpass", COMBOBOX_FILTER_BASE + (int)Filter::FILTER_MODE_HIGHPASS);
    filterMode.addItem("Bandpass", COMBOBOX_FILTER_BASE + (int)Filter::FILTER_MODE_BANDPASS);
    filterMode.setSelectedId(COMBOBOX_FILTER_BASE + (int)Filter::FILTER_MODE_LOWPASS, dontSendNotification);
    filterMode.addListener(this);

    filterEnvelopeAmountLabel.setText("Env", dontSendNotification);
    filterEnvelopeAmountLabel.setEditable(false);

    filterEnvelopeAmountSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    filterEnvelopeAmountSlider.setRange(-1.0, 1.0, 0.001);
    filterEnvelopeAmountSlider.setValue(0.0, dontSendNotification);
    filterEnvelopeAmountSlider.setLookAndFeel(&knobLookAndFeel);
    filterEnvelopeAmountSlider.addListener(this);

    filterLFOAmountLabel.setText("LFO", dontSendNotification);
    filterLFOAmountLabel.setEditable(false);

    filterLFOAmountSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    filterLFOAmountSlider.setRange(-1.0, 1.0, 0.001);
    filterLFOAmountSlider.setValue(0.0, dontSendNotification);
    filterLFOAmountSlider.setLookAndFeel(&knobLookAndFeel);
    filterLFOAmountSlider.addListener(this);

    // Filter envelope
    filterEnvelopeAttackLabel.setText("A", dontSendNotification);
    filterEnvelopeAttackLabel.setJustificationType(Justification::horizontallyCentred);
    filterEnvelopeAttackLabel.setEditable(false);

    filterEnvelopeAttackSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    filterEnvelopeAttackSlider.setRange(0.01, 10.0, 0.001);
    filterEnvelopeAttackSlider.setValue(0.01, dontSendNotification);
    filterEnvelopeAttackSlider.setLookAndFeel(&knobLookAndFeel);
    filterEnvelopeAttackSlider.addListener(this);

    filterEnvelopeDecayLabel.setText("D", dontSendNotification);
    filterEnvelopeDecayLabel.setJustificationType(Justification::horizontallyCentred);
    filterEnvelopeDecayLabel.setEditable(false);

    filterEnvelopeDecaySlider.setSliderStyle(Slider::RotaryVerticalDrag);
    filterEnvelopeDecaySlider.setRange(0.01, 15.0, 0.001);
    filterEnvelopeDecaySlider.setValue(0.5, dontSendNotification);
    filterEnvelopeDecaySlider.setLookAndFeel(&knobLookAndFeel);
    filterEnvelopeDecaySlider.addListener(this);

    filterEnvelopeSustainLabel.setText("S", dontSendNotification);
    filterEnvelopeSustainLabel.setJustificationType(Justification::horizontallyCentred);
    filterEnvelopeSustainLabel.setEditable(false);

    filterEnvelopeSustainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    filterEnvelopeSustainSlider.setRange(0.001, 1.0, 0.001);
    filterEnvelopeSustainSlider.setValue(0.1, dontSendNotification);
    filterEnvelopeSustainSlider.setLookAndFeel(&knobLookAndFeel);
    filterEnvelopeSustainSlider.addListener(this);

    filterEnvelopeReleaseLabel.setText("R", dontSendNotification);
    filterEnvelopeReleaseLabel.setJustificationType(Justification::horizontallyCentred);
    filterEnvelopeReleaseLabel.setEditable(false);

    filterEnvelopeReleaseSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    filterEnvelopeReleaseSlider.setRange(0.001, 15.0, 0.001);
    filterEnvelopeReleaseSlider.setValue(1.0, dontSendNotification);
    filterEnvelopeReleaseSlider.setLookAndFeel(&knobLookAndFeel);
    filterEnvelopeReleaseSlider.addListener(this);

    // LFO
    lfoModeLabel.setText("Wave", dontSendNotification);
    lfoModeLabel.setEditable(false);

    lfoMode.setEditableText(false);
    lfoMode.addItem("Sine",  COMBOBOX_LFO_BASE + (int)Oscillator::OSCILLATOR_MODE_SINE);
    lfoMode.addItem("Saw", COMBOBOX_LFO_BASE + (int)Oscillator::OSCILLATOR_MODE_SAW);
    lfoMode.addItem("Square", COMBOBOX_LFO_BASE + (int)Oscillator::OSCILLATOR_MODE_SQUARE);
    lfoMode.addItem("Triangle", COMBOBOX_LFO_BASE + (int)Oscillator::OSCILLATOR_MODE_TRIANGLE);
    lfoMode.setSelectedId(COMBOBOX_LFO_BASE + (int)Oscillator::OSCILLATOR_MODE_SINE, dontSendNotification);
    lfoMode.addListener(this);

    lfoFrequencyLabel.setText("Frq", dontSendNotification);
    lfoFrequencyLabel.setJustificationType(Justification::horizontallyCentred);
    lfoFrequencyLabel.setEditable(false);

    lfoFrequencySlider.setSliderStyle(Slider::RotaryVerticalDrag);
    lfoFrequencySlider.setRange(0.01, 10.0, 0.001);
    lfoFrequencySlider.setValue(0.01, dontSendNotification);
    lfoFrequencySlider.setLookAndFeel(&knobLookAndFeel);
    lfoFrequencySlider.addListener(this);

    // Main
    addAndMakeVisible(&oscillatorOneModeLabel);
    addAndMakeVisible(&oscillatorOneMode);
    addAndMakeVisible(&oscillatorOnePitchModLabel);
    addAndMakeVisible(&oscillatorOnePitchModSlider);
    addAndMakeVisible(&oscillatorMixLabel);
    addAndMakeVisible(&oscillatorMixSlider);
    addAndMakeVisible(&oscillatorTwoModeLabel);
    addAndMakeVisible(&oscillatorTwoMode);
    addAndMakeVisible(&oscillatorTwoPitchModLabel);
    addAndMakeVisible(&oscillatorTwoPitchModSlider);
    // Volume envelope
    addAndMakeVisible(&attackLabel);
    addAndMakeVisible(&attackSlider);
    addAndMakeVisible(&decayLabel);
    addAndMakeVisible(&decaySlider);
    addAndMakeVisible(&sustainLabel);
    addAndMakeVisible(&sustainSlider);
    addAndMakeVisible(&releaseLabel);
    addAndMakeVisible(&releaseSlider);
    // Filter
    addAndMakeVisible(&cutoffLabel);
    addAndMakeVisible(&cutoffSlider);
    addAndMakeVisible(&resonanceLabel);
    addAndMakeVisible(&resonanceSlider);
    addAndMakeVisible(&filterModeLabel);
    addAndMakeVisible(&filterMode);
    addAndMakeVisible(&filterEnvelopeAmountLabel);
    addAndMakeVisible(&filterEnvelopeAmountSlider);
    addAndMakeVisible(&filterLFOAmountLabel);
    addAndMakeVisible(&filterLFOAmountSlider);
    // Filter envelope
    addAndMakeVisible(&filterEnvelopeAttackLabel);
    addAndMakeVisible(&filterEnvelopeAttackSlider);
    addAndMakeVisible(&filterEnvelopeDecayLabel);
    addAndMakeVisible(&filterEnvelopeDecaySlider);
    addAndMakeVisible(&filterEnvelopeSustainLabel);
    addAndMakeVisible(&filterEnvelopeSustainSlider);
    addAndMakeVisible(&filterEnvelopeReleaseLabel);
    addAndMakeVisible(&filterEnvelopeReleaseSlider);
    // LFO
    addAndMakeVisible(&lfoModeLabel);
    addAndMakeVisible(&lfoMode);
    addAndMakeVisible(&lfoFrequencyLabel);
    addAndMakeVisible(&lfoFrequencySlider);
}

SynthAudioProcessorEditor::~SynthAudioProcessorEditor()
{
}

//==============================================================================
void SynthAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (Colours::white);

    //g.setColour (Colours::black);
    //g.setFont (15.0f);
    //g.drawFittedText ("Hello World!", getLocalBounds(), Justification::centred, 1);
}

void SynthAudioProcessorEditor::resized()
{
    // Main
    oscillatorOneModeLabel.setBounds(5, 0, 40, 12);
    oscillatorOneMode.setBounds(5, 12, 60, 28);
    oscillatorOnePitchModLabel.setBounds(70, 0, 40, 12);
    oscillatorOnePitchModSlider.setBounds(70, 12, 28, 28);

    oscillatorMixLabel.setBounds(110, 0, 40, 12);
    oscillatorMixSlider.setBounds(110, 12, 28, 28);

    oscillatorTwoModeLabel.setBounds(150, 0, 40, 12);
    oscillatorTwoMode.setBounds(150, 12, 60, 28);
    oscillatorTwoPitchModLabel.setBounds(215, 0, 40, 12);
    oscillatorTwoPitchModSlider.setBounds(215, 12, 28, 28);

    // Volume envelope
    attackLabel.setBounds(5, 50, 28, 12);
    attackSlider.setBounds(5, 62, 28, 28);

    decayLabel.setBounds(35, 50, 28, 12);
    decaySlider.setBounds(35, 62, 28, 28);

    sustainLabel.setBounds(65, 50, 28, 12);
    sustainSlider.setBounds(65, 62, 28, 28);

    releaseLabel.setBounds(95, 50, 28, 12);
    releaseSlider.setBounds(95, 62, 28, 28);

    // Filter
    cutoffLabel.setBounds(270, 0, 40, 12);
    cutoffSlider.setBounds(270, 12, 28, 28);

    resonanceLabel.setBounds(300, 0, 40, 12);
    resonanceSlider.setBounds(300, 12, 28, 28);

    filterModeLabel.setBounds(330, 0, 40, 12);
    filterMode.setBounds(330, 12, 100, 28);

    filterEnvelopeAmountLabel.setBounds(440, 0, 40, 12);
    filterEnvelopeAmountSlider.setBounds(440, 12, 28, 28);

    filterLFOAmountLabel.setBounds(470, 0, 40, 12);
    filterLFOAmountSlider.setBounds(470, 12, 28, 28);

    // Filter envelope
    filterEnvelopeAttackLabel.setBounds(270, 50, 28, 12);
    filterEnvelopeAttackSlider.setBounds(270, 62, 28, 28);

    filterEnvelopeDecayLabel.setBounds(300, 50, 28, 12);
    filterEnvelopeDecaySlider.setBounds(300, 62, 28, 28);

    filterEnvelopeSustainLabel.setBounds(330, 50, 28, 12);
    filterEnvelopeSustainSlider.setBounds(330, 62, 28, 28);

    filterEnvelopeReleaseLabel.setBounds(360, 50, 28, 12);
    filterEnvelopeReleaseSlider.setBounds(360, 62, 28, 28);

    // LFO
    lfoModeLabel.setBounds(5, 100, 40, 12);
    lfoMode.setBounds(5, 112, 100, 28);
    lfoFrequencyLabel.setBounds(115, 100, 28, 12);
    lfoFrequencySlider.setBounds(115, 112, 28, 28);
}

void SynthAudioProcessorEditor::sliderValueChanged(Slider* slider)
{
    // Main
    if (slider == &oscillatorOnePitchModSlider) {
        processor.onParamsChanged(OSCILLATOR_ONE_PITCHMOD, slider->getValue());
    }
    if (slider == &oscillatorMixSlider) {
        processor.onParamsChanged(OSCILLATOR_MIX, slider->getValue());
    }
    if (slider == &oscillatorTwoPitchModSlider) {
        processor.onParamsChanged(OSCILLATOR_TWO_PITCHMOD, slider->getValue());
    }

    // Volume envelope
    if (slider == &attackSlider) {
        processor.onParamsChanged(VOLUME_ENVELOPE_ATTACK, slider->getValue());
    }
    if (slider == &decaySlider) {
        processor.onParamsChanged(VOLUME_ENVELOPE_DECAY, slider->getValue());
    }
    if (slider == &sustainSlider) {
        processor.onParamsChanged(VOLUME_ENVELOPE_SUSTAIN, slider->getValue());
    }
    if (slider == &releaseSlider) {
        processor.onParamsChanged(VOLUME_ENVELOPE_RELEASE, slider->getValue());
    }

    // Filter
    if (slider == &cutoffSlider) {
        processor.onParamsChanged(FILTER_CUTOFF, slider->getValue());
    }
    if (slider == &resonanceSlider) {
        processor.onParamsChanged(FILTER_RESONANCE, slider->getValue());
    }

    // Filter envelope
    if (slider == &filterEnvelopeAttackSlider) {
        processor.onParamsChanged(FILTER_ENVELOPE_ATTACK, slider->getValue());
    }
    if (slider == &filterEnvelopeDecaySlider) {
        processor.onParamsChanged(FILTER_ENVELOPE_DECAY, slider->getValue());
    }
    if (slider == &filterEnvelopeSustainSlider) {
        processor.onParamsChanged(FILTER_ENVELOPE_SUSTAIN, slider->getValue());
    }
    if (slider == &filterEnvelopeReleaseSlider) {
        processor.onParamsChanged(FILTER_ENVELOPE_RELEASE, slider->getValue());
    }

    if (slider == &filterEnvelopeAmountSlider) {
        processor.onParamsChanged(FILTER_ENVELOPE_AMOUNT, slider->getValue());
    }
    if (slider == &filterLFOAmountSlider) {
        processor.onParamsChanged(FILTER_LFO_AMOUNT, slider->getValue());
    }

    // LFO
    if (slider == &lfoFrequencySlider) {
        processor.setLFOFrequency(slider->getValue());
    }
}

void SynthAudioProcessorEditor::comboBoxChanged(ComboBox* comboBox)
{
    // Oscillator
    if (comboBox == &oscillatorOneMode) {
        int mode = comboBox->getSelectedId() - COMBOBOX_OSCILLATOR_BASE;
        processor.onParamsChanged(OSCILLATOR_ONE_MODE, mode);
    }
    if (comboBox == &oscillatorTwoMode) {
        int mode = comboBox->getSelectedId() - COMBOBOX_OSCILLATOR_BASE;
        processor.onParamsChanged(OSCILLATOR_TWO_MODE, mode);
    }

    // Filter
    if (comboBox == &filterMode) {
        int mode = comboBox->getSelectedId() - COMBOBOX_FILTER_BASE;
        processor.onParamsChanged(FILTER_MODE, mode);
    }

    // LFO
    if (comboBox == &lfoMode) {
        Oscillator::OscillatorMode mode = static_cast<Oscillator::OscillatorMode>(comboBox->getSelectedId() - COMBOBOX_LFO_BASE);
        processor.setLFOMode(mode);
    }
}


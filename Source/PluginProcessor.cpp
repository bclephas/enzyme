/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
SynthAudioProcessor::SynthAudioProcessor()
{
    setProcessingPrecision(AudioProcessor::singlePrecision);

    lfo.setMode(Oscillator::OSCILLATOR_MODE_TRIANGLE);
    lfo.setFrequency(6.0);

    midiReceiver.noteOn.Connect(&voiceManager, &VoiceManager::onNoteOn);
    midiReceiver.noteOff.Connect(&voiceManager, &VoiceManager::onNoteOff);
}

SynthAudioProcessor::~SynthAudioProcessor()
{
}

//==============================================================================
const String SynthAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool SynthAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool SynthAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool SynthAudioProcessor::silenceInProducesSilenceOut() const
{
    return false;
}

double SynthAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int SynthAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int SynthAudioProcessor::getCurrentProgram()
{
    return 0;
}

void SynthAudioProcessor::setCurrentProgram (int index)
{
}

const String SynthAudioProcessor::getProgramName (int index)
{
    return String();
}

void SynthAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void SynthAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    voiceManager.setSampleRate(sampleRate);

    String msg;
    msg << "SampleRate " << sampleRate;
    Logger::getCurrentLogger()->writeToLog(msg);
}

void SynthAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void SynthAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    for (int i = getTotalNumInputChannels(); i < getTotalNumOutputChannels(); ++i) {
        buffer.clear(i, 0, buffer.getNumSamples());
    }

    // process all midiMessages to the midiReceiver
    MidiMessage m;
    int time;
    for (MidiBuffer::Iterator i (midiMessages); i.getNextEvent(m, time); ) {
        midiReceiver.onMessageReceived(m);
    }

    // process all samples
    float* leftChannelData  = buffer.getWritePointer(0);
    float* rightChannelData = buffer.getWritePointer(1);

    for (int sample = 0; sample < buffer.getNumSamples(); ++sample) {
        midiReceiver.next();
        leftChannelData[sample] = rightChannelData[sample] = static_cast<float>(voiceManager.nextSample());
    }

    midiReceiver.flush();
}

//==============================================================================
bool SynthAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* SynthAudioProcessor::createEditor()
{
    return new SynthAudioProcessorEditor (*this);
}

//==============================================================================
void SynthAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void SynthAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

void SynthAudioProcessor::onParamsChanged(int parameter, double value)
{
    using std::placeholders::_1;

    VoiceManager::VoiceParameterChangerFunction changer;

    String name;
    switch (parameter) {
        case SynthAudioProcessorEditor::OSCILLATOR_ONE_MODE:
            changer = std::bind(&VoiceManager::setOscillatorMode,
                        &voiceManager,
                        _1,
                        1,
                        static_cast<Oscillator::OscillatorMode>(value));
            name = "Oscillator 1 mode";
            break;
        case SynthAudioProcessorEditor::OSCILLATOR_ONE_PITCHMOD:
            changer = std::bind(&VoiceManager::setOscillatorPitchMod,
                        &voiceManager,
                        _1,
                        1,
                        value);
            name = "Oscillator 1 pitch mod";
            break;
        case SynthAudioProcessorEditor::OSCILLATOR_MIX:
            changer = std::bind(&VoiceManager::setOscillatorMix,
                        &voiceManager,
                        _1,
                        value);
            name = "Oscillator mix";
            break;
        case SynthAudioProcessorEditor::OSCILLATOR_TWO_MODE:
            changer = std::bind(&VoiceManager::setOscillatorMode,
                        &voiceManager,
                        _1,
                        2,
                        static_cast<Oscillator::OscillatorMode>(value));
            name = "Oscillator 2 mode";
            break;
        case SynthAudioProcessorEditor::OSCILLATOR_TWO_PITCHMOD:
            changer = std::bind(&VoiceManager::setOscillatorPitchMod,
                        &voiceManager,
                        _1,
                        2,
                        value);
            name = "Oscillator 2 pitch mod";
            break;
        case SynthAudioProcessorEditor::VOLUME_ENVELOPE_ATTACK:
            changer = std::bind(&VoiceManager::setVolumeEnvelopeStageValue,
                        &voiceManager,
                        _1,
                        EnvelopeGenerator::ENVELOPE_STAGE_ATTACK,
                        static_cast<Filter::FilterMode>(value));
            name = "Vol Env A";
            break;
        case SynthAudioProcessorEditor::VOLUME_ENVELOPE_DECAY:
            changer = std::bind(&VoiceManager::setVolumeEnvelopeStageValue,
                        &voiceManager,
                        _1,
                        EnvelopeGenerator::ENVELOPE_STAGE_DECAY,
                        static_cast<Filter::FilterMode>(value));
            name = "Vol Env D";
            break;
        case SynthAudioProcessorEditor::VOLUME_ENVELOPE_SUSTAIN:
            changer = std::bind(&VoiceManager::setVolumeEnvelopeStageValue,
                        &voiceManager,
                        _1,
                        EnvelopeGenerator::ENVELOPE_STAGE_SUSTAIN,
                        static_cast<Filter::FilterMode>(value));
            name = "Vol Env S";
            break;
        case SynthAudioProcessorEditor::VOLUME_ENVELOPE_RELEASE:
            changer = std::bind(&VoiceManager::setVolumeEnvelopeStageValue,
                        &voiceManager,
                        _1,
                        EnvelopeGenerator::ENVELOPE_STAGE_RELEASE,
                        static_cast<Filter::FilterMode>(value));
            name = "Vol Env R";
            break;
        case SynthAudioProcessorEditor::FILTER_MODE:
            changer = std::bind(&VoiceManager::setFilterMode,
                        &voiceManager,
                        _1,
                        static_cast<Filter::FilterMode>(value));
            name = "Filter mode";
            break;
        case SynthAudioProcessorEditor::FILTER_CUTOFF:
            changer = std::bind(&VoiceManager::setFilterCutoff,
                        &voiceManager,
                        _1,
                        value);
            name = "Filter cutoff";
            break;
        case SynthAudioProcessorEditor::FILTER_RESONANCE:
            changer = std::bind(&VoiceManager::setFilterResonance,
                        &voiceManager,
                        _1,
                        value);
            name = "Filter resonance";
            break;
        case SynthAudioProcessorEditor::FILTER_ENVELOPE_AMOUNT:
            changer = std::bind(&VoiceManager::setFilterEnvelopeAmount,
                        &voiceManager,
                        _1,
                        value);
            name = "Filter env amount";
            break;
        case SynthAudioProcessorEditor::FILTER_LFO_AMOUNT:
            changer = std::bind(&VoiceManager::setFilterLFOAmount,
                        &voiceManager,
                        _1,
                        value);
            name = "Filter lfo amount";
            break;
        case SynthAudioProcessorEditor::FILTER_ENVELOPE_ATTACK:
            changer = std::bind(&VoiceManager::setFilterEnvelopeStageValue,
                        &voiceManager,
                        _1,
                        EnvelopeGenerator::ENVELOPE_STAGE_ATTACK,
                        static_cast<Filter::FilterMode>(value));
            name = "Filter env A";
            break;
        case SynthAudioProcessorEditor::FILTER_ENVELOPE_DECAY:
            changer = std::bind(&VoiceManager::setFilterEnvelopeStageValue,
                        &voiceManager,
                        _1,
                        EnvelopeGenerator::ENVELOPE_STAGE_DECAY,
                        static_cast<Filter::FilterMode>(value));
            name = "Filter env D";
            break;
        case SynthAudioProcessorEditor::FILTER_ENVELOPE_SUSTAIN:
            changer = std::bind(&VoiceManager::setFilterEnvelopeStageValue,
                        &voiceManager,
                        _1,
                        EnvelopeGenerator::ENVELOPE_STAGE_SUSTAIN,
                        static_cast<Filter::FilterMode>(value));
            name = "Filter env S";
            break;
        case SynthAudioProcessorEditor::FILTER_ENVELOPE_RELEASE:
            changer = std::bind(&VoiceManager::setFilterEnvelopeStageValue,
                        &voiceManager,
                        _1,
                        EnvelopeGenerator::ENVELOPE_STAGE_RELEASE,
                        static_cast<Filter::FilterMode>(value));
            name = "Filter env R";
            break;
        default:
            /* do nothing */
            break;
    }

    String msg;
    msg << "Param update: " << name << ": " << value;
    Logger::getCurrentLogger()->writeToLog(msg);

    voiceManager.updateVoices(changer);
}

void SynthAudioProcessor::setLFOMode(Oscillator::OscillatorMode mode)
{
    voiceManager.setLFOMode(mode);

    String msg;
    msg << "LFO mode " << mode;
    Logger::getCurrentLogger()->writeToLog(msg);
}

void SynthAudioProcessor::setLFOFrequency(double frequency)
{
    voiceManager.setLFOFrequency(frequency);

    String msg;
    msg << "LFO frequency " << frequency;
    Logger::getCurrentLogger()->writeToLog(msg);
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new SynthAudioProcessor();
}


/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */ 

#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "KnobLookAndFeel.h"

//==============================================================================
/**
*/
class SynthAudioProcessorEditor  : public AudioProcessorEditor
                                 , private Slider::Listener
                                 , private ComboBox::Listener
{
public:
    enum Parameters
    {
        FREQUENCY = 0,
        LFO_MODE,
        LFO_FREQUENCY,

        NumParameters
    };

    enum VoiceParameters
    {
        OSCILLATOR_ONE_MODE = Parameters::NumParameters + 1,
        OSCILLATOR_ONE_PITCHMOD,
        OSCILLATOR_MIX,
        OSCILLATOR_TWO_MODE,
        OSCILLATOR_TWO_PITCHMOD,
        VOLUME_ENVELOPE_ATTACK,
        VOLUME_ENVELOPE_DECAY,
        VOLUME_ENVELOPE_SUSTAIN,
        VOLUME_ENVELOPE_RELEASE,
        FILTER_CUTOFF,
        FILTER_RESONANCE,
        FILTER_MODE,
        FILTER_ENVELOPE_AMOUNT,
        FILTER_LFO_AMOUNT,
        FILTER_ENVELOPE_ATTACK,
        FILTER_ENVELOPE_DECAY,
        FILTER_ENVELOPE_SUSTAIN,
        FILTER_ENVELOPE_RELEASE,
    };

public:
    SynthAudioProcessorEditor (SynthAudioProcessor&);
    ~SynthAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    void sliderValueChanged(Slider* slider) override;
    void comboBoxChanged(ComboBox* combobox) override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    SynthAudioProcessor& processor;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SynthAudioProcessorEditor)

    // Main
    Label oscillatorOneModeLabel;
    ComboBox oscillatorOneMode;
    Label oscillatorOnePitchModLabel;
    Slider oscillatorOnePitchModSlider;

    Label oscillatorMixLabel;
    Slider oscillatorMixSlider;

    Label oscillatorTwoModeLabel;
    ComboBox oscillatorTwoMode;
    Label oscillatorTwoPitchModLabel;
    Slider oscillatorTwoPitchModSlider;

    // Volume Envelope
    Label attackLabel;
    Slider attackSlider;
    Label decayLabel;
    Slider decaySlider;
    Label sustainLabel;
    Slider sustainSlider;
    Label releaseLabel;
    Slider releaseSlider;

    // Filter
    Label cutoffLabel;
    Slider cutoffSlider;
    Label resonanceLabel;
    Slider resonanceSlider;
    Label filterModeLabel;
    ComboBox filterMode;
    Label filterEnvelopeAmountLabel;
    Slider filterEnvelopeAmountSlider;
    Label filterLFOAmountLabel;
    Slider filterLFOAmountSlider;

    // Filter envelope
    Label filterEnvelopeAttackLabel;
    Slider filterEnvelopeAttackSlider;
    Label filterEnvelopeDecayLabel;
    Slider filterEnvelopeDecaySlider;
    Label filterEnvelopeSustainLabel;
    Slider filterEnvelopeSustainSlider;
    Label filterEnvelopeReleaseLabel;
    Slider filterEnvelopeReleaseSlider;

    // LFO
    Label lfoModeLabel;
    ComboBox lfoMode;
    Label lfoFrequencyLabel;
    Slider lfoFrequencySlider;

    KnobLookAndFeel knobLookAndFeel;
};


#endif  // PLUGINEDITOR_H_INCLUDED

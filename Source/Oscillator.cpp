/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "Oscillator.h"

const double PI = 2.0 * std::acos(0.0);
const double TWOPI = 2.0 * PI;

Oscillator::Oscillator()
    : oscillatorMode(OSCILLATOR_MODE_SINE)
    , frequency(440.0)
    , sampleRate(44100.0)
    , phase(0.0)
    , phaseIncrement(0.0)
    , pitchMod(0.0)
{
    updateIncrement();
}

void Oscillator::reset()
{
    phase = 0.0;
}

void Oscillator::setMode(OscillatorMode mode)
{
    oscillatorMode = mode;
}

void Oscillator::setFrequency(double frequency)
{
    this->frequency = frequency;
    updateIncrement();
}

double Oscillator::getFrequency() const
{
    return frequency;
}

void Oscillator::setSampleRate(double sampleRate)
{
    this->sampleRate = sampleRate;
    updateIncrement();
}

void Oscillator::setPitchMod(double amount)
{
    pitchMod = amount;
    updateIncrement();
}

double Oscillator::nextSample()
{
    double value = 0.0;

    switch (oscillatorMode) {
        default:
        case OSCILLATOR_MODE_SINE:
            value = std::sin(phase);
            break;
        case OSCILLATOR_MODE_SAW:
            value = 1.0 - (2.0 * phase / TWOPI);
            break;
        case OSCILLATOR_MODE_SQUARE:
            if (phase <= PI) {
                value = 1.0;
            } else {
                value = -1.0;
            }
            break;
        case OSCILLATOR_MODE_TRIANGLE:
            value = -1.0 + (2.0 * phase / TWOPI);
            value = 2.0 * (std::fabs(value) - 0.5);
            break;
    }

    phase += phaseIncrement;
    while (phase >= TWOPI) {
        phase -= TWOPI;
    }
    return value;
}

void Oscillator::updateIncrement()
{
    double pitchModAsFrequency = std::pow(2.0, std::fabs(pitchMod) * 14.0) - 1;
    if (pitchMod < 0) {
        pitchModAsFrequency = -pitchModAsFrequency;
    }
    double calculatedFrequency = std::fmin(std::fmax(frequency + pitchModAsFrequency, 0.0), sampleRate / 2.0);
    phaseIncrement = calculatedFrequency * TWOPI / sampleRate;
}


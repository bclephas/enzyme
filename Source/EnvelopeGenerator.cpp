/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "EnvelopeGenerator.h"

EnvelopeGenerator::EnvelopeGenerator()
    : minimumLevel(0.0001)
    , currentStage(ENVELOPE_STAGE_OFF)
    , currentLevel(minimumLevel)
    , sampleRate(0.0)
    , multiplier(1.0)
    , currentSampleIndex(0)
    , nextStageSampleIndex(0)
{
    stageValue[ENVELOPE_STAGE_OFF]     = 0.0;
    stageValue[ENVELOPE_STAGE_ATTACK]  = 0.01;
    stageValue[ENVELOPE_STAGE_DECAY]   = 0.5;
    stageValue[ENVELOPE_STAGE_SUSTAIN] = 0.1;
    stageValue[ENVELOPE_STAGE_RELEASE] = 1.0;
}

void EnvelopeGenerator::reset()
{
    currentStage = ENVELOPE_STAGE_OFF;
    currentLevel = minimumLevel;
    multiplier = 1.0;
    currentSampleIndex = 0;
    nextStageSampleIndex = 0;
}

void EnvelopeGenerator::enterStage(EnvelopeStage stage)
{
    if (currentStage == stage) {
        return;
    }

    if (currentStage == ENVELOPE_STAGE_OFF) {
        startedEnvelopeCycle();
    }
    if (stage == ENVELOPE_STAGE_OFF) {
        finishedEnvelopeCycle();
    }

    currentStage = stage;
    currentSampleIndex = 0;

    if (currentStage == ENVELOPE_STAGE_OFF ||
        currentStage == ENVELOPE_STAGE_SUSTAIN) {
        nextStageSampleIndex = 0;
    } else {
        // calculate how long (in samples) this stage should last
        nextStageSampleIndex = stageValue[currentStage] * sampleRate;
    }

    switch (stage) {
        case ENVELOPE_STAGE_OFF:
            // set level to zero, i.e. silence
            currentLevel = 0.0;
            multiplier = 1.0;
            break;
        case ENVELOPE_STAGE_ATTACK:
            // start very silent and transition to 1.0
            currentLevel = minimumLevel;
            calculateMultiplier(currentLevel, 1.0, nextStageSampleIndex);
            break;
        case ENVELOPE_STAGE_DECAY:
            // fall from current level to the sustain level (and ensure it won't reach absolute zero)
            currentLevel = 1.0;
            calculateMultiplier(currentLevel, std::fmax(stageValue[ENVELOPE_STAGE_SUSTAIN], minimumLevel), nextStageSampleIndex);
            break;
        case ENVELOPE_STAGE_SUSTAIN:
            // hold the level value
            currentLevel = stageValue[ENVELOPE_STAGE_SUSTAIN];
            multiplier = 1.0;
            break;
        case ENVELOPE_STAGE_RELEASE:
            // decay from currentlevel to silence
            // Possibly go from ATTACK or DECAY to RELEASE, so do not change currentLevel
            calculateMultiplier(currentLevel, minimumLevel, nextStageSampleIndex);
            break;
        default:
            /* do nothing */
            break;
    }
}

double EnvelopeGenerator::nextSample()
{
    // generator is in ATTACK, DECAY or RELEASE stage
    if (currentStage != ENVELOPE_STAGE_OFF &&
        currentStage != ENVELOPE_STAGE_SUSTAIN) {
        if (currentSampleIndex == nextStageSampleIndex) {
            EnvelopeStage newStage = static_cast<EnvelopeStage>((currentStage + 1) % NumEnvelopeStages);
            enterStage(newStage);
        }
        currentLevel *= multiplier;
        ++currentSampleIndex;
    }
    return currentLevel;
}

void EnvelopeGenerator::setSampleRate(double sampleRate)
{
    this->sampleRate = sampleRate;
}

EnvelopeGenerator::EnvelopeStage EnvelopeGenerator::getCurrentStage() const
{
    return currentStage;
}

void EnvelopeGenerator::setEnvelope(EnvelopeStage stage, double value)
{
    stageValue[stage] = value;

    if (stage == currentStage) {
        // recalculate multiplier and nextStageSampleIndex
        if (currentStage == ENVELOPE_STAGE_ATTACK
        ||	currentStage == ENVELOPE_STAGE_DECAY
        ||	currentStage == ENVELOPE_STAGE_RELEASE) {
            double nextLevelValue;
            switch (currentStage) {
                case ENVELOPE_STAGE_ATTACK:
                    nextLevelValue = 1.0;
                    break;
                case ENVELOPE_STAGE_DECAY:
                    nextLevelValue = std::fmax(stageValue[ENVELOPE_STAGE_SUSTAIN], minimumLevel);
                    break;
                case ENVELOPE_STAGE_RELEASE:
                    nextLevelValue = minimumLevel;
                    break;
                default:
                    /* do nothing */
                    break;
            }

            if (nextStageSampleIndex == 0) {
                nextStageSampleIndex = 1;
            }
            double currentStageProcess = currentSampleIndex / nextStageSampleIndex;
            double remainingStageProcess = 1.0 - currentStageProcess;

            unsigned long long samplesUntilNextStage = remainingStageProcess * value * sampleRate;

            nextStageSampleIndex = currentSampleIndex + samplesUntilNextStage;
            calculateMultiplier(currentLevel, nextLevelValue, samplesUntilNextStage);
        } else if (currentStage == ENVELOPE_STAGE_SUSTAIN) {
            currentLevel = value;
        }
    }

    if (currentStage == ENVELOPE_STAGE_DECAY && stage == ENVELOPE_STAGE_SUSTAIN) {
        unsigned long long samplesUntilNextStage = nextStageSampleIndex - currentSampleIndex;

        calculateMultiplier(currentLevel, std::fmax(stageValue[ENVELOPE_STAGE_SUSTAIN], minimumLevel), samplesUntilNextStage);
    }
}

double EnvelopeGenerator::getEnvelope(EnvelopeStage stage) const
{
    return stageValue[stage];
}

void EnvelopeGenerator::calculateMultiplier(double startLevel, double endLevel, unsigned long long lengthInSamples)
{
    if (lengthInSamples == 0) {
        lengthInSamples = 1;
    }
    // calculate the multiplier based on two values (levels) and the given time (lengthInSamples)
    // use this value to multiply this value with the envelope value (making it exponential)
    multiplier = 1.0 + (std::log(endLevel) - std::log(startLevel)) / lengthInSamples;
}


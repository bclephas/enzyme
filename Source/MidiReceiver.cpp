/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "MidiReceiver.h"

#include <cmath>

MidiReceiver::MidiReceiver()
    : numKeys(0)
{
    memset(keyStatus, false, sizeof(keyStatus));
}

void MidiReceiver::next()
{
    while (!buffer.empty()) {
        MidiMessage& message = buffer.front();

        int noteNumber = message.getNoteNumber();
        int velocity = message.getVelocity();

        if (message.isNoteOn() && velocity) {
            if (keyStatus[noteNumber] == false) {
                keyStatus[noteNumber] = true;
                numKeys += 1;

                noteOn(noteNumber, velocity);
            }
        } else {
            if (keyStatus[noteNumber] == true) {
                keyStatus[noteNumber] = false;
                numKeys -= 1;

                noteOff(noteNumber, velocity);
            }
        }

        buffer.pop();
    }
}

void MidiReceiver::onMessageReceived(MidiMessage& message)
{
    // Only add noteOn/noteOff for now
    if (message.isNoteOn() || message.isNoteOff()) {
        buffer.push(message);
    }
}

void MidiReceiver::flush()
{
    while (buffer.size() > 0) {
        buffer.pop();
    }
}


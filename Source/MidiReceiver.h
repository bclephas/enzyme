/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#ifndef MidiReceiver_h
#define MidiReceiver_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "Signal.h"
#include <queue>

class MidiReceiver
{
public:
    MidiReceiver();

    void next();
    void onMessageReceived(MidiMessage& message);
    void flush();

public:
    static const int keyCount = 128;
    std::queue<MidiMessage> buffer;
    int numKeys;
    bool keyStatus[keyCount];

    Gallant::Signal2<int, int> noteOn;
    Gallant::Signal2<int, int> noteOff;
};

#endif // MidiReceiver_h


/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#ifndef VOICE_H
#define VOICE_H

#include "EnvelopeGenerator.h"
#include "Filter.h"
#include "Oscillator.h"

class Voice
{
    friend class VoiceManager;

public:
    Voice();

    double nextSample();

    void reset();
    void setNoteNumber(int noteNumber);
    void setFilterEnvelopeAmount(double amount);
    void setFilterLFOAmount(double amount);
    void setOscillatorOnePitchAmount(double amount);
    void setOscillatorTwoPitchAmount(double amount);
    void setOscillatorMix(double mix);
    void setLFOValue(double value);

private:
    void onFinishedEnvelopeCycle();

private:
    Oscillator oscillatorOne;
    Oscillator oscillatorTwo;
    EnvelopeGenerator volumeEnvelope;
    EnvelopeGenerator filterEnvelope;
    Filter filter;

    int noteNumber;
    int velocity;

    double filterEnvelopeAmount;
    double oscillatorMix;
    double filterLFOAmount;
    double oscillatorOnePitchAmount;
    double oscillatorTwoPitchAmount;
    double lfoValue;

    bool isActive;
};

#endif // VOICE_H


/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "VoiceManager.h"
#include "EnvelopeGenerator.h"

VoiceManager::VoiceManager()
{
}

double VoiceManager::nextSample()
{
    double sample = 0.0;
    double lfoValue = lfo.nextSample();
    for (VoicesArray::iterator it = voices.begin(); it != voices.end(); ++it) {
        Voice& voice = *it;
        voice.setLFOValue(lfoValue);
        sample += voice.nextSample();
    }
    return sample;
}

void VoiceManager::setSampleRate(double sampleRate)
{
    for (VoicesArray::iterator it = voices.begin(); it != voices.end(); ++it) {
        Voice& voice = *it;
        voice.oscillatorOne.setSampleRate(sampleRate);
        voice.oscillatorTwo.setSampleRate(sampleRate);
        voice.volumeEnvelope.setSampleRate(sampleRate);
        voice.filterEnvelope.setSampleRate(sampleRate);
    }
    lfo.setSampleRate(sampleRate);
}

void VoiceManager::setLFOMode(Oscillator::OscillatorMode mode)
{
    lfo.setMode(mode);
}

void VoiceManager::setLFOFrequency(double frequency)
{
    lfo.setFrequency(frequency);
}

void VoiceManager::updateVoices(VoiceParameterChangerFunction changer)
{
    for (VoicesArray::iterator it = voices.begin(); it != voices.end(); ++it) {
        Voice& voice = *it;
        changer(voice);
    }
}

void VoiceManager::onNoteOn(int noteNumber, int velocity)
{
    Voice* pVoice = findFirstAvailableVoice();
    if (pVoice == nullptr) {
        return;
    }

    pVoice->reset();
    pVoice->setNoteNumber(noteNumber);
    pVoice->velocity = velocity;
    pVoice->isActive = true;
    pVoice->volumeEnvelope.enterStage(EnvelopeGenerator::ENVELOPE_STAGE_ATTACK);
    pVoice->filterEnvelope.enterStage(EnvelopeGenerator::ENVELOPE_STAGE_ATTACK);
}

void VoiceManager::onNoteOff(int noteNumber, int velocity)
{
    for (VoicesArray::iterator it = voices.begin(); it != voices.end(); ++it) {
        Voice& voice = *it;
        if (voice.isActive && voice.noteNumber == noteNumber) {
            voice.volumeEnvelope.enterStage(EnvelopeGenerator::ENVELOPE_STAGE_RELEASE);
            voice.filterEnvelope.enterStage(EnvelopeGenerator::ENVELOPE_STAGE_RELEASE);
        }
    }
}

// voice parameter change functions
void VoiceManager::setVolumeEnvelopeStageValue(Voice& voice, EnvelopeGenerator::EnvelopeStage stage, double value)
{
    voice.volumeEnvelope.setEnvelope(stage, value);
}

void VoiceManager::setFilterEnvelopeStageValue(Voice& voice, EnvelopeGenerator::EnvelopeStage stage, double value)
{
    voice.filterEnvelope.setEnvelope(stage, value);
}

void VoiceManager::setOscillatorMode(Voice& voice, int oscillatorNumber, Oscillator::OscillatorMode mode)
{
    switch (oscillatorNumber) {
        case 1:
            voice.oscillatorOne.setMode(mode);
            break;
        case 2:
            voice.oscillatorTwo.setMode(mode);
            break;
        default:
            /* do nothing */
            break;
    }
}

void VoiceManager::setOscillatorPitchMod(Voice& voice, int oscillatorNumber, double amount)
{
    switch (oscillatorNumber) {
        case 1:
            voice.setOscillatorOnePitchAmount(amount);
            break;
        case 2:
            voice.setOscillatorTwoPitchAmount(amount);
            break;
        default:
            /* do nothing */
            break;
    }
}

void VoiceManager::setOscillatorMix(Voice& voice, double mix)
{
    voice.setOscillatorMix(mix);
}

void VoiceManager::setFilterCutoff(Voice& voice, double cutoff)
{
    voice.filter.setCutoff(cutoff);
}

void VoiceManager::setFilterResonance(Voice& voice, double resonance)
{
    voice.filter.setResonance(resonance);
}

void VoiceManager::setFilterMode(Voice& voice, Filter::FilterMode mode)
{
    voice.filter.setFilterMode(mode);
}

void VoiceManager::setFilterEnvelopeAmount(Voice& voice, double amount)
{
    voice.setFilterEnvelopeAmount(amount);
}

void VoiceManager::setFilterLFOAmount(Voice& voice, double amount)
{
    voice.setFilterLFOAmount(amount);
}

Voice* VoiceManager::findFirstAvailableVoice()
{
    Voice* pVoice = nullptr;
    for (VoicesArray::iterator it = voices.begin(); it != voices.end(); ++it) {
        if (!it->isActive) {
            pVoice = it;
            break;
        }
    }
    return pVoice;
}


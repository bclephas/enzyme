# Enzyme

Softsynth as VSTi

Only tested on Linux, however due to the use of JUCE
it should compile on all major platforms.

# Dependencies
* JUCE 4.1+
* Steinberg's VST SDK

# Acknowledgement

Martin Finke for the instructions of a basic soft synth.

Plugin uses a signals and slots implementation by Patric Hogan.
JKnobMan is used to generate the UI knobs.

rotate knob orignally created by 16slice, its called knob_med_red
led knob originally created by VirtualAnalogy

